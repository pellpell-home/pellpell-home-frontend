import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  srcDir: "client/",
  app: {
    head: {
      title: "ぺるぺるのものづくり",
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { name: 'description', content: 'ぺるぺるのホームページ' },
        { name: "apple-mobile-web-app-title", content: "pellpell-home" },
        { name: "application-name", content: "pellpell-home" },
        { name: "msapplication-TileColor", content: "#ffc107" },
        { name: "theme-color", content: "#ffffff" },
        // OGP
        {
          hid: 'og:site_name',
          property: 'og:site_name',
          content: 'pellpell.net',
        },
        { hid: 'og:type', property: 'og:type', content: 'website' },
        { hid: 'og:url', property: 'og:url', content: 'https://pellpell.net/' },
        {
          property: 'og:title',
          content: 'ぺるぺるのものづくり',
        },
        {
          property: 'og:description',
          content: 'ぺるぺるのホームページ',
        },
        {
          property: 'og:image',
          content: 'https://pellpell.net/image/pellpell.png',
        },
        { name: 'twitter:card', content: 'summary' },
      ],
      link: [
        { rel: "icon", type: 'image/x-icon', href: '/favicon.ico' },
        { rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png" },
        { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png" },
        { rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png" },
        { rel: "manifest", href: "/site.webmanifest" },
        { rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#ffc107" },
      ],
    },
  },
  runtimeConfig: {
    public: {
      gaId: ''
    }
  },
  modules: [
    '@nuxt/content'
  ],
  content: {
    // https://content.nuxtjs.org/api/configuration
    highlight: {
      theme: "dracula-soft"
    }
  },
  css: ["vuetify/lib/styles/main.sass"],
  build: {
    transpile: ["vuetify"],
  },
  vite: {
    define: {
      'process.env.DEBUG': false
    },
  },
  ssr: true,
})
