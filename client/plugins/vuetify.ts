import '@mdi/font/css/materialdesignicons.css'
import { createVuetify, ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const lightPrimaryColor = '#FFECB3' // amber-lighten-4
const darkPrimaryColor = '#664C00'

const lightInfoColor = '#E1F5FE' // blue-lighten-5
const darkInfoColor = '#022D3F'

const lightLinkColor = '#2962FF' // blue-accent-4
const darkLinkColor = '#80D8FF' // light-blue-accent-1

const lightSecondaryColor = '#FAFAFA' // grey-lighten-5
const darkSecondaryColor = '#212121' // grey-darken-4

const appLightTheme: ThemeDefinition = {
  dark: false,
  colors: {
    primary: lightPrimaryColor,
    info: lightInfoColor,
    link: lightLinkColor,
    secondary: lightSecondaryColor,
  }
}

const appDarkTheme: ThemeDefinition = {
  dark: true,
  colors: {
    primary: darkPrimaryColor,
    info: darkInfoColor,
    link: darkLinkColor,
    secondary: darkSecondaryColor,
  }
}

export default defineNuxtPlugin(nuxtApp => {
  const vuetify = createVuetify({
    components,
    directives,
    theme: {
      defaultTheme: 'appLightTheme',
      themes: {
        appLightTheme,
        appDarkTheme
      }
    }
  })

  nuxtApp.vueApp.use(vuetify)
})
