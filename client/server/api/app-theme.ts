import { useCookie , setCookie } from 'h3'

export default (req, res) => {
  
  
  let appTheme = useCookie(req, 'appTheme') || '0'

  if (appTheme == '0') {
    return { appTheme }
  }

  appTheme = appTheme == 'light' ? 'dark' : 'light'
  setCookie(res, 'appTheme', appTheme)
  console.log("setCookie:", appTheme);
  

  // Send JSON response
  return { appTheme }
}
