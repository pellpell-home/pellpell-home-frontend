import { useCookie, setCookie } from 'h3'

export default (req, res) => {
  const appTheme = 'light'
  setCookie(res, 'appTheme', appTheme)
  console.log("setCookie:", appTheme);
  return { appTheme }
}
